//
//  Constants.swift
//  NimbleTestApp
//
//  Created by Hoang Le on 2/7/20.
//  Copyright © 2020 Hoang Le. All rights reserved.
//
struct Number {
    static let perpage = 10
}
struct APPURL {
    struct Domains {
        static let endPoint = "https://nimble-survey-api.herokuapp.com"
        
    }
    
    struct Routes {
        static let SurVeys = "/surveys.json"
        static let Token = "/oauth/token"
    }
}
