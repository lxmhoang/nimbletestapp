//
//  Protocols.swift
//  NimbleTestApp
//
//  Created by Hoang Le on 2/7/20.
//  Copyright © 2020 Hoang Le. All rights reserved.
//

import Foundation
import UIKit

protocol RemoteDataLoadersProtocol {
    func getSurveyAtIndex(index:Int, completion: @escaping (SurveyDetail?, Int?, Error?) -> Void)
    func refreshSurveyList() -> Void
}

