//
//  SurveyListViewController.swift
//  NimbleTestApp
//
//  Created by Hoang Le on 2/5/20.
//  Copyright © 2020 Hoang Le. All rights reserved.
//

import UIKit

class SurveyListViewController: UIViewController {
    var curIndex = 0
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backGroundImageView: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    weak var curSurvey: SurveyDetail?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSwipe()
        let angle = CGFloat.pi/2
        pageControl.transform = CGAffineTransform(rotationAngle: angle)
        displayCurSurvey()
        // Do any additional setup after loading the view.
    }
    

    
    /*
    // MARK: - Private Functions
    */
    
    private func addSwipe() {
        let directions: [UISwipeGestureRecognizer.Direction] = [.right, .left, .up, .down]
        for direction in directions {
            let gesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
            gesture.direction = direction
            self.view.addGestureRecognizer(gesture)
        }
    }

    @objc private func handleSwipe(sender: UISwipeGestureRecognizer) {
        if (sender.direction  == .up || sender.direction  == .left ) {
            curIndex+=1
        }
        if (sender.direction  == .down || sender.direction  == .right ) {
            curIndex = curIndex > 0 ? curIndex-1 : 0
        }
        displayCurSurvey()
    }
    
    private func displayCurSurvey() {
        pageControl.currentPage = curIndex
        spinner.startAnimating()
        weak var weakself = self
        DataLoader.sharedNetworkManager.getSurveyAtIndex(index: curIndex, completion: {survey, surveyCount, error in
            
            if error == nil, let survey = survey {
                weakself!.curSurvey = survey
                DispatchQueue.main.async {
                    weakself!.spinner.stopAnimating()
                    weakself!.pageControl.numberOfPages = surveyCount ?? 0
                    if let data = survey.imageData {
                        self.backGroundImageView.image = UIImage(data: data)
                    }
                    weakself!.titleLabel.text = survey.title
                    weakself!.desLabel.text = survey.description
                }
            }else {
                // something wrong, handle here by preseting some alert or retry ...
            }
        }
        )
        
    }


    /*
    // MARK: - Btn Actions
    */
    @IBAction func refreshBtnTapped(_ sender: Any) {
        DataLoader.sharedNetworkManager.refreshSurveyList()
        curIndex = 0
        displayCurSurvey()
    }
    
    
    @IBAction func takeSurveyBtnTapped(_ sender: Any) {
        guard curSurvey != nil else  {return}
        let vc = TakeSurveyViewController(nibName: "TakeSurveyViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
