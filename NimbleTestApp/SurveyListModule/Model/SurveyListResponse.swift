//
//  SurveyListResponse.swift
//  NimbleTestApp
//
//  Created by Hoang Le on 2/7/20.
//  Copyright © 2020 Hoang Le. All rights reserved.
//

import ObjectMapper

class SurveyDetail : Mappable {
    var id, title, description, cover_image_url : String?
    var imageData: Data?
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        description <- map["description"]
        cover_image_url <- map["cover_image_url"]
    }
}
