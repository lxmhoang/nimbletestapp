//
//  DataLoader.swift
//  NimbleTestApp
//
//  Created by Hoang Le on 2/7/20.
//  Copyright © 2020 Hoang Le. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper


class DataLoader : RemoteDataLoadersProtocol{
    
    private var accessToken: String = ""
    private var oauth2Handler : OauthHandler
    public var session : Session
    static let sharedNetworkManager = DataLoader()
    private var surveyDict = [Int: SurveyDetail]()
    
    public init(){
        oauth2Handler = OauthHandler()
        session = Session(interceptor: oauth2Handler)
    }
    
    func getSurveyAtIndex(index: Int, completion: @escaping (SurveyDetail?, Int? ,Error?) -> Void) {
        if (surveyDict.keys.contains(index)) {
            completion(surveyDict[index], surveyDict.keys.count ,nil)
        }else {
            
            weak var weakself = self
            let page : Int = index/Number.perpage
            getSurveyList(page: page, perPage: Number.perpage) { (subIndexSurvey, surveyRetrieved, error) in
                if let  weakself = weakself {
                    if (error == nil && subIndexSurvey > -1){
                        
                        let surveyIndex = subIndexSurvey + page * Number.perpage
                        weakself.surveyDict[surveyIndex] = surveyRetrieved
                        
                        if surveyIndex == index {
                            completion(surveyRetrieved, weakself.surveyDict.keys.count, nil)
                        }
                    }else {
                        completion(surveyRetrieved, weakself.surveyDict.keys.count, error)
                    }
                    
                }
            }
        }
    }
    
    
    func refreshSurveyList() {
        surveyDict = [:]
    }


    private func getSurveyList(page: Int, perPage : Int, completion: @escaping (Int, SurveyDetail?, Error?) -> Void) {
   
        
        session.request(APPURL.Domains.endPoint+APPURL.Routes.SurVeys + "?page=\(page)&per_page=\(perPage)").responseArray{
            (response : DataResponse<[SurveyDetail], AFError>) in
            if let surveyListResponse = response.value, response.error == nil {
                for (index, survey) in surveyListResponse.enumerated() {
                    if let urlString = survey.cover_image_url, !urlString.isEmpty {
                        let url = URL(string: urlString )
                                           let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in

                                               if let data = data, error == nil{
                                                   survey.imageData = data
                                               }
                                               completion(index, survey, nil)
                                           }
                                           task.resume()
                    }
                }
            }else {
                completion(-1, nil, response.error)
            }
        }
        
    }
    
    


}

class OauthHandler : RequestInterceptor {
    
    private typealias RefreshCompletion = (_ succeeded: Bool, _ accessToken: String?) -> Void;
    private var isRefreshing = false
    private var requestsToRetry: [(RetryResult) -> Void] = []
    var accessToken:String? = ""
    
    private let session: Session = {
           let configuration = URLSessionConfiguration.default
           return Session(configuration: configuration)
       }()

    
    private let lock = NSLock()
    
    // MARK: -  request interceptor protocol, retry & adapt func
    
    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
          lock.lock() ; defer { lock.unlock() }

              if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
                  requestsToRetry.append(completion)

                  if !isRefreshing {
                      refreshTokens { [weak self] succeeded, accessToken in
                          guard let strongSelf = self else { return }

                          strongSelf.lock.lock() ; defer { strongSelf.lock.unlock() }

                          if let accessToken = accessToken {
                              strongSelf.accessToken = accessToken
                          }
                        

                        strongSelf.requestsToRetry.forEach { $0(.retry) }
                          strongSelf.requestsToRetry.removeAll()
                      }
                  }
              } else {
                completion(.doNotRetry)
              }
    }
    
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(APPURL.Domains.endPoint),
            let token = accessToken
        {
               var adaptedRequest = urlRequest
               adaptedRequest.setValue("Bearer " + token, forHTTPHeaderField: "Authorization")

                completion(.success(adaptedRequest))
            
        } else{
            completion(.success(urlRequest))
            
        }

       }
    
  
    // MARK: - refresh token
    
    private func refreshTokens(completion: @escaping RefreshCompletion) {
        guard !isRefreshing else { return }
        
        isRefreshing = true
        
        let urlString = APPURL.Domains.endPoint + APPURL.Routes.Token
        
        let parameters : [String : Any] = [
            "grant_type" : "password",
            "username" : "carlos@nimbl3.com",
            "password" : "antikera"
            
        ];
        
        session.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { [weak self] response in
            guard let strongSelf = self else { return }
            if
                let json = response.value as? [String: Any],
                let accessToken = json["access_token"] as? String
            {
                debugPrint(accessToken)
                completion(true, accessToken)
            } else {
                completion(false, nil)
            }
            strongSelf.isRefreshing = false
        }
    }
   
}
