//
//  UnitTestDataLoader.swift
//  NimbleTestAppTests
//
//  Created by Hoang Le on 2/5/20.
//  Copyright © 2020 Hoang Le. All rights reserved.
//

import NimbleTestApp
import XCTest
import OHHTTPStubs




let instance : DataLoader = DataLoader.sharedNetworkManager

class UnitTestDataLoader: XCTestCase {
    


    override func setUp() {
      
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        HTTPStubs.removeAllStubs()
        instance.refreshSurveyList()
    }
    
    func testDataLoaderWithSimpleCorrectResponse() {
        stub(condition: isHost("nimble-survey-api.herokuapp.com")) { _ in
                  let stubPath = OHPathForFile("goodjson.json", type(of: self))
                  return fixture(filePath: stubPath!, headers: ["Content-Type":"application/json"])
                        .requestTime(0.0, responseTime:OHHTTPStubsDownloadSpeedWifi)
                }
        let expectation = self.expectation(description: "Good")
        
        var count = 0
        for i in 0...10 {
            instance.getSurveyAtIndex(index: i, completion: { survey, surveyCount,error in
                XCTAssertNil(error, "Got error")
                XCTAssertNotNil(survey, "survey must not be nil")
                
                count += 1
                
                // since goodjson.json only has 6 object, maximum value of i should be 5. Any i that bigger than 5 should never returns
                if i == 5 {
                    expectation.fulfill()
                    
                }
                
            })
            
        }
        
        waitForExpectations(timeout: 150, handler: nil)
    }
    
    
    func testDataLoaderWithSimpleBadResponse() {
        stub(condition: isHost("nimble-survey-api.herokuapp.com")) { _ in
                  let stubPath = OHPathForFile("dummy.json", type(of: self))
                  return fixture(filePath: stubPath!, headers: ["Content-Type":"application/json"])
                        .requestTime(0.0, responseTime:OHHTTPStubsDownloadSpeedWifi)
                }
        let expectation = self.expectation(description: "Good")
        
        var count = 0
        for i in 0...10 {
            instance.getSurveyAtIndex(index: i, completion: { survey, surveyCount,error in
                XCTAssertNotNil(error, "Should return an error because this is dummy response")
                XCTAssertNil(survey, "No survey should be returned since this is dummy response")
                count += 1
                if i == 5 {
                    expectation.fulfill()
                }
                
            })
            
        }
        
        waitForExpectations(timeout: 150, handler: nil)
        
    }



}
